﻿using System;

namespace CurrencyTools.Domain
{
    public class AccountTransaction
    {
        public Guid Id { get; set; }
        public decimal Value { get; set; }
        public DateTime Date { get; set; }
    }
}
