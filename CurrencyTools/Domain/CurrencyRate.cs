﻿using System;

namespace CurrencyTools.Domain
{
    public class CurrencyRate
    {
        public Currency Currency { get; set; }
        public DateTime Date { get; set; }
        public decimal Rate { get; set; }
        public int Multiplicity { get; set; }
    }
}
