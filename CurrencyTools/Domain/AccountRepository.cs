﻿using System;
using System.Linq;

namespace CurrencyTools.Domain
{
    public class AccountRepository
    {
        public Currency[] Currencies { get; set; }
        public CurrencyRate[] CurrencyRates { get; set; }
        public Account[] Accounts { get; set; }
        public AccountRepository()
        {
            Init();
        }

        public Currency GetCurrency(int id) => Currencies.FirstOrDefault(c => c.Id == id);
        public Currency GetCurrency(string code) => Currencies.FirstOrDefault(c => c.Code == code.ToUpper());

        public void AddTransaction(Account account, string currencyCode, decimal value, DateTime? date = null)
        {
            var currency = GetCurrency(currencyCode);
            var currencyRate = CurrencyRates.Where(r => r.Currency == currency).OrderByDescending(r => r.Date).First();
            var currencyRateAccount = account.Currency == currency
                ? currencyRate
                : CurrencyRates.Where(r => r.Currency == account.Currency).OrderByDescending(r => r.Date).First();
            var transaction = new AccountTransaction
            {
                Id = Guid.NewGuid(),
                Date = date ?? DateTime.Now,
                Value = account.Currency == currency
                    ? value
                    : (value * currencyRate.Rate / currencyRate.Multiplicity) * currencyRateAccount.Multiplicity /
                      currencyRateAccount.Rate
            };
            account.AccountTransactions.Add(transaction);
        }

        private void Init()
        {
            Currencies = new[]
            {
                new Currency {Id = 643, Code = "RUB", Name = "Российский рубль"},
                new Currency {Id = 978, Code = "EUR", Name = "Евро"},
                new Currency {Id = 840, Code = "USD", Name = "Доллар США"}
            };
            var rub = Currencies[0];
            var eur = Currencies[1];
            var usd = Currencies[2];
            CurrencyRates = new[]
            {
                // 14.09.2021
                new CurrencyRate {Currency = rub, Date = DateTime.MinValue, Rate = 1M, Multiplicity = 1},
                new CurrencyRate {Currency = eur, Date = DateTime.MinValue, Rate = 86.1150M, Multiplicity = 1},
                new CurrencyRate {Currency = usd, Date = DateTime.MinValue, Rate = 73.0841M, Multiplicity = 1},
                // 15.09.2021
                new CurrencyRate {Currency = eur, Date = DateTime.Today, Rate = 85.9880M, Multiplicity = 1},
                new CurrencyRate {Currency = usd, Date = DateTime.Today, Rate = 72.7171M, Multiplicity = 1},
            };
            Accounts = new[]
            {
                new Account {Number = "1000", Currency = rub},
                new Account {Number = "1001", Currency = eur},
                new Account {Number = "1002", Currency = usd}
            };
        }
    }
}
