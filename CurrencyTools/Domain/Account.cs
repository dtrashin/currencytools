﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CurrencyTools.Domain
{
    public class Account
    {
        public string Number { get; set; }
        public Currency Currency { get; set; }
        public List<AccountTransaction> AccountTransactions { get; set; } = new List<AccountTransaction>();

        public decimal GetBalance(DateTime? dateTime = null)
        {
            var result = dateTime == null
                ? AccountTransactions.Sum(t => t.Value)
                : AccountTransactions.Where(t => t.Date <= dateTime).Sum(t => t.Value);

            return result;
        }

        public string GetBalanceString(DateTime? dateTime = null)
        {
            return $"Account #{Number}, Balance: {GetBalance(dateTime):N} {Currency.Code}";
        }
    }
}
