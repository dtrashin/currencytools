﻿using System;
using System.Linq;
using CurrencyTools.Domain;

namespace CurrencyTools
{
    class Program
    {
        static void Main(string[] args)
        {
            var accountRepository = new AccountRepository();
            var accountRub = accountRepository.Accounts.First(a => a.Number == "1000");
            Console.WriteLine(accountRub.GetBalanceString());
            accountRepository.AddTransaction(accountRub, "USD", 100, DateTime.Today.AddDays(-3));
            Console.WriteLine(accountRub.GetBalanceString());
            accountRepository.AddTransaction(accountRub, "RUB", 100, DateTime.Today.AddDays(-1));
            Console.WriteLine(accountRub.GetBalanceString());
            accountRepository.AddTransaction(accountRub, "EUR", 100);
            Console.WriteLine(accountRub.GetBalanceString());

        }
    }
}
